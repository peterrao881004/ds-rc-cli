const download = require("download-git-repo");
const downloadUrl = require("download");
const chalk = require("chalk");
const open = require("open");
const { changeFile, rmFile } = require("./tools");
const log = (content) => console.log(chalk.green(content));

/**
 * clone项目
 * @param {*} repo
 * @param {*} desc
 */
const clone = async (repo, desc) => {
  //repo 地址 ， desc 放的位置
  const ora = require("ora");
  const process = ora(`下载... ${repo} ${desc}`);
  process.start();
  return new Promise((resolve) => {
    download(repo, desc, { clone: true }, (err) => {
      if (err) {
        console.log(err);
        process.fail("下载失败");
        resolve(false);
      } else {
        process.succeed("下载成功");
        resolve(true);
      }
    });
  });
};

const downloadZip = async (repo, desc) => {
  //repo 地址 ， desc 放的位置
  const ora = require("ora");
  const process = ora(`下载... ${repo} ${desc}`);
  process.start();
  // token 9年后过期
  let downloadOptions = {
    extract: true,
    strip: 1,
    mode: "666",
    headers: {
      accept: "application/zip",
    },
  };
  try {
    await downloadUrl(repo, desc, downloadOptions);
    process.succeed("下载成功");
  } catch (e) {
    console.log(e);
    process.fail("下载失败");
    throw { msg: "下载失败" };
  }
};

// const clone = async () => {
//   const { exec } = require('child_process');
//   let gitUrl = "git@gitlab.wuxingdev.cn:bfe/web/ds-rc-cli.git";
//   let cmdStr = `git clone -b develop ${gitUrl} ${name}`;
//   exec(cmdStr, (error, stdout, stderr) => {
//     if (error) {
//       console.log(error);
//     }
//     console.log(chalk.green("代码模版已生成!"));
//   });
// }

const spawn = async (...args) => {
  const { spawn } = require("child_process");
  return new Promise((resolve) => {
    const proc = spawn(...args);
    proc.stdout.pipe(process.stdout);
    proc.stderr.pipe(process.stderr);
    proc.on("close", () => {
      resolve();
    });
  });
};

/**
 * install 初始化项目
 * @param {*} name
 */
const install = async (name) => {
  log("安装依赖中... ", name);

  // //设置私服
  // await spawn("npm", ["set", "registry", "http://10.200.106.149:4873"], {
  // 	cwd: `./${name}`
  // });

  //安装依赖
  await spawn("tyarn", [], {
    cwd: `./${name}`,
  });

  //安装完成
  log(
    chalk.green(
      `👌安装完成:
	To get Start: ===========================
	cd ${name}
			npm start
	===========================`
    )
  );

  //打开
  open("http://localhost:8000/login");
  await spawn("npm", ["start"], {
    cwd: `./${name}`,
  });
};

/**
 * 修改文件名称
 * @param {*} src
 * @param {*} name
 */
const initPackage = (src, name) => {
  console.log("initPackage", src, name);
  rmFile(`${src}/ds-ap-template`);
  changeFile(`${src}/package.json`, (data) => {
    let _data = JSON.parse(data.toString());
    _data.name = name;
    _data.version = "1.0.0";
    return JSON.stringify(_data, null, 2);
  });
};

module.exports = { clone, install, initPackage, downloadZip };
