const fs = require("fs");
const util = require("util");
const path = require("path");

/**
 * 读取文件，并修改文件
 * @param {*} _src
 * @param {*} changData
 */
const changeFile = (_src, changData) => {
  try {
    const data = fs.readFileSync(_src);
    const str = changData(data);
    str && fs.writeFileSync(_src, str);
  } catch {
    throw { msg: "读写文件失败" };
  }
};

/**
 * 删除
 * @param {*} _src
 */
const rmFile = (_src) => {
  let files = fs.readdirSync(_src);
  for (var i = 0; i < files.length; i++) {
    let newPath = path.join(_src, files[i]);
    let stat = fs.statSync(newPath);
    if (stat.isDirectory()) {
      //如果是文件夹就递归下去
      rmFile(newPath);
    } else {
      //删除文件
      fs.unlinkSync(newPath);
    }
  }
  //如果文件夹是空的，就将自己删除掉
  fs.rmdirSync(_src);
};

/**
 * 首字母大写
 * @param {*} param0
 */
const firstUpperCase = ([first, ...rest]) =>
  first.toUpperCase() + rest.join("");
const firstLowerCase = ([first, ...rest]) =>
  first.toLowerCase() + rest.join("");

/**
 * 判断文件夹是存在
 * @param {*} src
 */
const checkFile = async (src) => {
  try {
    const stats = await util.promisify(fs.stat)(src);
    if (stats.isDirectory()) {
      return true;
    }
    return false;
  } catch (e) {
    return false;
  }
};

const toHump = (data) => {
  return data.map((item) => {
    return firstUpperCase(item);
  });
};

const formatName = (name) => {
  const result = name.split("-");
  if (result.length > 1) {
    const hump = toHump(result).join("");
    return {
      typeName: name.toUpperCase().split("-").join("_"),
      capitalHump: toHump(result).join(""), //驼峰，首字母大写
      littleHump: firstLowerCase(hump), //驼峰，搜字幕小写
      routerName: result.join("_"),
      name,
    };
  } else {
    return {
      typeName: name.toUpperCase(),
      capitalHump: firstUpperCase(name),
      littleHump: name,
      routerName: name,
      name,
    };
  }
};

module.exports = {
  changeFile,
  rmFile,
  firstUpperCase,
  checkFile,
  formatName,
};
