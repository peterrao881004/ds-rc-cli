
const chalk = require("chalk");
const log = content => console.log(chalk.green(content));
const errorMsg = content => console.log(chalk.red(content));
const {
	checkProject,
	createPage,
	// createRouter,
	// createTypes,
	// createReducer,
	// createSaga,
} = require("../template");
const { formatName } = require("../utils/tools");

module.exports = async (obj) => {
	const { 
		name, 
		// saga, 
		// reducer,
	} = obj;
	log(`🚀创建页面: ${name}`);
	try {
		//检查项目是ts还是js，检查page是否已经存在
		const { type = "ts", page } = await checkProject(name);
		if (!page) return;
		const objName = formatName(name);
		//创建page
		await createPage({ type, objName, ...obj });

		// //创建router
		// await createRouter(type, objName);

		// //创建types
		// await createTypes({ type, objName, ...obj });

		// //创建saga
		// saga && (await createSaga({ type, objName, ...obj }));

		// // //创建reducer
		// reducer && (await createReducer({ type, objName, ...obj }));
		log("页面创建完成");
		log(`You can add a config in config/routes.ts like following
		{
			name: '${name}',
			path: '/${name}',
			component: './${name}',
		},`);
	} catch (e) {
		errorMsg(e.msg || e);
	}
};