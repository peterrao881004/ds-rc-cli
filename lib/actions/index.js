const init = require("./create_project");
const page = require("./create_page");
module.exports = {
	init,
	page
};