const path = require("path");
const { install, initPackage, downloadZip } = require("../utils/create_project");
// const { copy } = require('./utils/copy');

const chalk = require("chalk");
const log = content => console.log(chalk.green(content));
const errorMsg = content => console.log(chalk.red(content));

const { promisify } = require("util");
const figlet = promisify(require("figlet"));

module.exports = async ({ name, branch }) => {
	const diliverData = await figlet("  ");
	log(`🐣 创建项目: ${name}`);

	// const dataSrc = path.resolve(__dirname, `../src/${branch}`);  //文件地址
	const goalSrc = path.resolve(`./${name}`);  //目标地址
	try {
		//初始化项目，copy代码
		// await copy(dataSrc, goalSrc);

		//clone 代码
		
		log(diliverData);
		log(`🚀 开始下载模板: ds-ap-template-${branch}`);
		log(diliverData);
		await downloadZip(
			// `https://github.com/reduction-admin/react-reduction/archive/refs/heads/master.zip`,
			// "http://192.168.0.103:8888/ds-ap-template.zip",
			"http://100.81.2.24:8888/ds-ap-template.zip",
			name);
		//修改文件名称
		initPackage(goalSrc, name);

		//安装依赖
		await install(name);
		log(diliverData);
		log("项目创建完成");
	} catch (e) {
		errorMsg(e.msg || e);
	}
};