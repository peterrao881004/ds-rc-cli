const init = require("./init");
const page = require("./page");
module.exports = {
	init,
	page
};