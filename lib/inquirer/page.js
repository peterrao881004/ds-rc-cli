#!/usr/bin/env node
const inquirer = require("inquirer");
const path = require("path");
const { checkFile } = require("../utils/tools");
const chalk = require("chalk");
const log = content => console.log(chalk.red(content));

const askQuestions = () => {
	const questions = [
		{
			name: "FILENAME",
			type: "input",
			message: "请输入页面名称?",
			validate: async (input) => {
				const reg = /^[a-zA-Z]{1}[a-zA-Z-0-9]{1,}$/;
				if (!reg.test(input)) {
					return "页面名不符合规范";
				}
				const isNorm = await checkFile(path.resolve() + `/src/pages/${input}`);
				if (isNorm) {
					return "文件已经存在";
				}
				return true;
			}
		},
		{
			type: "list",
			name: "TEMPLATE",
			message: "请选择页面模板",
			choices: [
				{ name: "Simple Page", value: "simple" },
				{ name: "Table Page", value: "table" },
			],
		},
		// {
		// 	type: "confirm",
		// 	name: "SAGAS",
		// 	message: "是否创建saga?",
		// 	default: true
		// },
		// {
		// 	type: "confirm",
		// 	name: "REDUCERS",
		// 	message: "是否创建reducer?",
		// 	default: true
		// }
	];
	return inquirer.prompt(questions);
};

const createFile = require("../init");
module.exports = async () => {
	// ask questions
	const isNorm = await checkFile(path.resolve() + "/src/pages");
	if (!isNorm) {
		log("项目路径错误");
		return;
	}

	const answers = await askQuestions();
	const { FILENAME, SAGAS, REDUCERS, TEMPLATE } = answers;
	createFile("page", {
		name: FILENAME,
		saga: SAGAS,
		reducer: REDUCERS,
		template: TEMPLATE
	});
};