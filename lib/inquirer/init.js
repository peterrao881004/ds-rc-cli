#!/usr/bin/env node
const inquirer = require("inquirer");
const path = require("path");
const { checkFile } = require("../utils/tools");

/**
 * 名称
 */
const askQuestionsName = () => {
	const questions = [
		{
			name: "PROJECT_NAME",
			type: "input",
			message: "请输入项目名称?",
			validate: async (input) => {
				const reg = /^[0-9a-zA-Z-]{1,}$/;
				const isNorm = await checkFile(path.resolve() + `/${input}`);
				if (!reg.test(input)) {
					return "输入不符合规范";
				}
				if (isNorm) {
					return "文件已经存在";
				}
				return true;
			},
		},
	];
	return inquirer.prompt(questions);
};

const askQuestionsTypeTel = () => {
	const questions = [
		{
			type: "list",
			name: "TEMPLATE",
			message: "请选择模版",
			choices: [
				// { name: "Mobile", value: false },
				{ name: "PC", value: "pc-js" },
			],
		},
	];
	return inquirer.prompt(questions);
};

const askQuestionsType = () => {
	const questions = [
		{
			type: "confirm",
			name: "TYPE",
			message: "是否用TS",
			default: true,
		},
	];
	return inquirer.prompt(questions);
};

const createFile = require("../init");

module.exports = async () => {
	// ask questions name
	const { PROJECT_NAME } = await askQuestionsName();
	const param = { name: PROJECT_NAME };
	// ask questions template
	const { TEMPLATE } = await askQuestionsTypeTel();
	if (TEMPLATE) {
		param.branch = TEMPLATE;
	} else {
		// ask questions type 只有h5 才有
		const { TYPE } = await askQuestionsType();
		param.branch = TYPE ? "h5-ts" : "h5-js";
	}
	createFile("init", param);
};
