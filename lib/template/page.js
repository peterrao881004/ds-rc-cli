
const fs = require("fs");
const { promisify } = require("util");
const writeFile = promisify(fs.writeFile);
const mkdir = promisify(fs.mkdir);
const path = require("path");
const getTel = require("./page_tel");
const getTelTs = require("./page_tel_ts");

module.exports = async (obj) => {
	const { type, name } = obj;
	const isJs = type === "js";
	const base_src = path.resolve() + `/src/pages/${name}/index.`;
	const file_src = base_src + (isJs ? "js" : "tsx");
	const css_src = base_src + "less";

	try {
		//创建页面问文件夹
		await mkdir(path.resolve() + `/src/pages/${name}`);
		const tel = isJs ? getTel(obj) : getTelTs(obj);
		//创建页面文件，css文件
		await writeFile(file_src, tel);
		await writeFile(css_src, "@charset \"UTF-8\";");
	} catch (e) {
		// log("页面创建失败");
		throw { msg: "页面创建失败，请检查路径" };
	}
};