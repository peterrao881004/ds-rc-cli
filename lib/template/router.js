
const path = require("path");
const { changeFile } = require("../utils/tools");

module.exports = async (type, objName) => {
	const { name, routerName } = objName;
	const suffix = type === "js" ? "js" : "ts";
	const file_src = path.resolve() + `/src/routes/config.${suffix}`;

	try {
		changeFile(file_src, (data) => {
			let _data = data.toString().split("[")[1].split("]")[0];
			if (_data.indexOf(`"/${routerName}"`) > -1) return null;
			const tel = `const routesConfig = [
	{
		path: "/${routerName}",
		page: "${name}",
	},${_data}];

export default routesConfig;
`;
			return tel;
		});
	} catch (e) {
		throw { msg: "router 创建失败" };
	}
};