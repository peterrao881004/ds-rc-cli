
const path = require("path");
const { changeFile } = require("../utils/tools");

module.exports = async (type, objName) => {
	const { capitalHump, name } = objName;
	const file_src = path.resolve() + `/src/api.${type}`;
	await changeFile(file_src, (data) => {
		let _data = data.toString();
		if (_data.indexOf(`fetch${capitalHump}Data`) > -1) return null;
		const tel = `${_data}
export const fetch${capitalHump}Data = "GET ../mock_data/${name}.json";
`;
		return tel;
	});
};