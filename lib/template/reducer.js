const fs = require("fs");
const { promisify } = require("util");
const writeFile = promisify(fs.writeFile);
const path = require("path");
const { changeFile } = require("../utils/tools");

const getTel = (obj) => {
	const { type, objName } = obj;
	const isJs = type === "js";
	const { littleHump, typeName, capitalHump } = objName;
	const typeCode = isJs ? "" : `, ${capitalHump}DataType, ${capitalHump}ActionType`;
	const paramCode = isJs ? "(state = null, action)" : `(state: ${capitalHump}DataType = null, action: ${capitalHump}ActionType): ${capitalHump}DataType`;
	const tel = `import { combineReducers } from "redux";
import assign from "lodash/assign";
import { RECEIVE_${typeName}_DATA${typeCode} } from "@types";

const ${littleHump}State = ${paramCode} => {
	switch (action.type) {
		case RECEIVE_${typeName}_DATA:
			return assign({}, state, {
				...action.payload,
			});
		default:
			return state;
	}
};

const ${littleHump}Reducer = combineReducers({
	${littleHump}State,
});

export default ${littleHump}Reducer;
`;

	return tel;
};

module.exports = async (obj) => {
	const { type, name, objName } = obj;
	const { littleHump } = objName;
	const file_src = path.resolve() + `/src/reducers/${name}.reducer.${type}`;
	const index_src = path.resolve() + `/src/reducers/index.${type}`;

	try {
		changeFile(index_src, (data) => {
			let _data = data.toString();
			if (_data.indexOf(`./${name}.reducer`) > -1) return null;
			const [imp, obj] = data.toString().split("export");
			const objStr = obj.split("{")[1].split("}")[0];
			const tel = `import ${littleHump}Reducer from "./${name}.reducer";
${imp}export {
	${littleHump}Reducer, ${objStr}};
`;
			return tel;
		});

		const tel = getTel(obj);

		await writeFile(file_src, tel);
	} catch (e) {
		throw { msg: "reducer 创建失败" };
	}
};