
const getDefaultTel = (res) => {
	const { typeName, littleHump, capitalHump, name } = res;
	const tel = `import React, { useEffect, ReactElement } from "react";
import { useAction, useStore } from "@hooks";
import { FETCH_${typeName}_DATA } from "@types";
import "./index.css";

function ${capitalHump}(): ReactElement {
	const fetchData = useAction(FETCH_${typeName}_DATA);
	const { ${littleHump}State } = useStore("${littleHump}Reducer");

	useEffect(() => {
		document.title = "${name}";
		fetchData();
	}, []);

	if (!${littleHump}State) return null;
	return <div>页面初始化完成:{${littleHump}State.result}</div>;
}

export default ${capitalHump};
`;
	return tel;
};

const getSadsel = (res) => {
	const { typeName, capitalHump, name } = res;
	const tel = `import React, { useEffect, useState, ReactElement } from "react";
import { useAction } from "@hooks";
import { FETCH_${typeName}_DATA } from "@types";
import "./index.css";

function ${capitalHump}(): ReactElement {
	const [data, setData] = useState(null);
	const fetchData = useAction(FETCH_${typeName}_DATA, { callback: (res) => setData(res) });

	useEffect(() => {
		document.title = "${name}";
		fetchData();
	}, []);

	if (!data) return null;
	return <div>页面初始化完成:{data.result}</div>;
}

export default ${capitalHump};
`;
	return tel;
};

const getReducerTel = (res) => {
	const { typeName, littleHump, capitalHump, name } = res;
	const tel = `import React, { useEffect, ReactElement } from "react";
import { useAction, useStore } from "@hooks";
import { RECEIVE_${typeName}_DATA } from "@types";
import "./index.css";

function ${capitalHump}(): ReactElement {
	const { ${littleHump}State } = useStore("${littleHump}Reducer");
	const updateData = useAction(RECEIVE_${typeName}_DATA);

	useEffect(() => {
		updateData({ data: "只是reducer" });
		document.title = "${name}";
	}, []);

	if (!${littleHump}State) return null;
	return <div>页面初始化完成:{${littleHump}State.data}</div>;
}

export default ${capitalHump};
`;
	return tel;
};

const getBaseTel = (res) => {
	const { capitalHump, name } = res;
	const tel = `import { memo, useEffect } from "react";
import "./index.less";

const ${capitalHump}: React.FC = () => {
	useEffect(() => {
		document.title = "${name}"
	}, []);

	return <div>${name}页面初始化完成</div>
}

export default memo(${capitalHump})
`;
	return tel;
};

const getTablePageTel = (res) => {
	const { capitalHump, name } = res;
	const tel = `import { addRule, removeRule, rule, updateRule } from '@/services/mock/api';
	import { PlusOutlined } from '@ant-design/icons';
	import type {
		ActionType,
		ProColumns,
		ProDescriptionsItemProps,
	} from '@ant-design/pro-components';
	import {
		FooterToolbar,
		ModalForm,
		PageContainer,
		ProDescriptions,
		ProFormText,
		ProFormTextArea,
		ProTable,
	} from '@ant-design/pro-components';
	import { Button, Drawer, Input, message } from 'antd';
	import React, { useRef, useState } from 'react';
	import { FormattedMessage, useIntl } from 'umi';
	
	/**
	 * @en-US Add node
	 * @zh-CN 添加节点
	 * @param fields
	 */
	const handleAdd = async (fields: API.RuleListItem) => {
		const hide = message.loading('正在添加');
		try {
			await addRule({ ...fields });
			hide();
			message.success('Added successfully');
			return true;
		} catch (error) {
			hide();
			message.error('Adding failed, please try again!');
			return false;
		}
	};
	
	/**
	 * @en-US Update node
	 * @zh-CN 更新节点
	 *
	 * @param fields
	 */
	const handleUpdate = async (fields: API.RuleListItem) => {
		const hide = message.loading('Configuring');
		try {
			await updateRule({
				name: fields.name,
				desc: fields.desc,
				key: fields.key,
			});
			hide();
	
			message.success('Configuration is successful');
			return true;
		} catch (error) {
			hide();
			message.error('Configuration failed, please try again!');
			return false;
		}
	};
	
	/**
	 *  Delete node
	 * @zh-CN 删除节点
	 *
	 * @param selectedRows
	 */
	const handleRemove = async (selectedRows: API.RuleListItem[]) => {
		const hide = message.loading('正在删除');
		if (!selectedRows) return true;
		try {
			await removeRule({
				key: selectedRows.map((row) => row.key),
			});
			hide();
			message.success('Deleted successfully and will refresh soon');
			return true;
		} catch (error) {
			hide();
			message.error('Delete failed, please try again');
			return false;
		}
	};
	
	const ${capitalHump}: React.FC = () => {
		/**
		 * @en-US Pop-up window of new window
		 * @zh-CN 新建窗口的弹窗
		 *  */
		const [createModalVisible, handleModalVisible] = useState<boolean>(false);
	
		const [showDetail, setShowDetail] = useState<boolean>(false);
	
		const actionRef = useRef<ActionType>();
		const [currentRow, setCurrentRow] = useState<API.RuleListItem>();
		const [selectedRowsState, setSelectedRows] = useState<API.RuleListItem[]>([]);
	
		/**
		 * @en-US International configuration
		 * @zh-CN 国际化配置
		 * */
		const intl = useIntl();
	
		const columns: ProColumns<API.RuleListItem>[] = [
			{
				title: (
					<FormattedMessage
						id="pages.searchTable.updateForm.ruleName.nameLabel"
						defaultMessage="Rule name"
					/>
				),
				dataIndex: 'name',
				tip: 'The rule name is the unique key',
				render: (dom, entity) => {
					return (
						<a
							onClick={() => {
								setCurrentRow(entity);
								setShowDetail(true);
							}}
						>
							{dom}
						</a>
					);
				},
			},
			{
				title: <FormattedMessage id="pages.searchTable.titleDesc" defaultMessage="Description" />,
				dataIndex: 'desc',
				valueType: 'textarea',
			},
			{
				title: (
					<FormattedMessage
						id="pages.searchTable.titleCallNo"
						defaultMessage="Number of service calls"
					/>
				),
				dataIndex: 'callNo',
				sorter: true,
				hideInForm: true,
			},
			{
				title: <FormattedMessage id="pages.searchTable.titleStatus" defaultMessage="Status" />,
				dataIndex: 'status',
				hideInForm: true,
				valueEnum: {
					0: {
						text: (
							<FormattedMessage
								id="pages.searchTable.nameStatus.default"
								defaultMessage="Shut down"
							/>
						),
						status: 'Default',
					},
					1: {
						text: (
							<FormattedMessage id="pages.searchTable.nameStatus.running" defaultMessage="Running" />
						),
						status: 'Processing',
					},
					2: {
						text: (
							<FormattedMessage id="pages.searchTable.nameStatus.online" defaultMessage="Online" />
						),
						status: 'Success',
					},
					3: {
						text: (
							<FormattedMessage
								id="pages.searchTable.nameStatus.abnormal"
								defaultMessage="Abnormal"
							/>
						),
						status: 'Error',
					},
				},
			},
			{
				title: (
					<FormattedMessage
						id="pages.searchTable.titleUpdatedAt"
						defaultMessage="Last scheduled time"
					/>
				),
				sorter: true,
				dataIndex: 'updatedAt',
				valueType: 'dateTime',
			},
			{
				title: <FormattedMessage id="pages.searchTable.titleOption" defaultMessage="Operating" />,
				dataIndex: 'option',
				valueType: 'option',
				render: (_, record) => [
					<a
						key="config"
						onClick={() => {
							setCurrentRow(record);
						}}
					>
						<FormattedMessage id="pages.searchTable.config" defaultMessage="Configuration" />
					</a>,
					<a key="subscribeAlert" href="">
						<FormattedMessage
							id="pages.searchTable.subscribeAlert"
							defaultMessage="Subscribe to alerts"
						/>
					</a>,
				],
			},
		];
	
		return (
			<PageContainer title="${name}" fixedHeader>
				<ProTable<API.RuleListItem, API.PageParams>
					headerTitle={intl.formatMessage({
						id: 'pages.searchTable.title',
						defaultMessage: 'Enquiry form',
					})}
					actionRef={actionRef}
					rowKey="key"
					search={{
						labelWidth: 120,
					}}
					toolBarRender={() => [
						<Button
							type="primary"
							key="primary"
							onClick={() => {
								handleModalVisible(true);
							}}
						>
							<PlusOutlined /> <FormattedMessage id="pages.searchTable.new" defaultMessage="New" />
						</Button>,
					]}
					pagination={{
						showQuickJumper: true
					}}
					request={rule}
					columns={columns}
					rowSelection={{
						onChange: (_, selectedRows) => {
							setSelectedRows(selectedRows);
						},
					}}
				/>
				{selectedRowsState?.length > 0 && (
					<FooterToolbar
						extra={
							<div>
								<FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen" />{' '}
								<a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
								<FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
								&nbsp;&nbsp;
								<span>
									<FormattedMessage
										id="pages.searchTable.totalServiceCalls"
										defaultMessage="Total number of service calls"
									/>{' '}
									{selectedRowsState.reduce((pre, item) => pre + item.callNo!, 0)}{' '}
									<FormattedMessage id="pages.searchTable.tenThousand" defaultMessage="万" />
								</span>
							</div>
						}
					>
						<Button
							onClick={async () => {
								await handleRemove(selectedRowsState);
								setSelectedRows([]);
								actionRef.current?.reloadAndRest?.();
							}}
						>
							<FormattedMessage
								id="pages.searchTable.batchDeletion"
								defaultMessage="Batch deletion"
							/>
						</Button>
						<Button type="primary">
							<FormattedMessage
								id="pages.searchTable.batchApproval"
								defaultMessage="Batch approval"
							/>
						</Button>
					</FooterToolbar>
				)}
				<ModalForm
					title={intl.formatMessage({
						id: 'pages.searchTable.createForm.newRule',
						defaultMessage: 'New rule',
					})}
					width="400px"
					visible={createModalVisible}
					onVisibleChange={handleModalVisible}
					onFinish={async (value) => {
						const success = await handleAdd(value as API.RuleListItem);
						if (success) {
							handleModalVisible(false);
							if (actionRef.current) {
								actionRef.current.reload();
							}
						}
					}}
				>
					<ProFormText
						rules={[
							{
								required: true,
								message: (
									<FormattedMessage
										id="pages.searchTable.ruleName"
										defaultMessage="Rule name is required"
									/>
								),
							},
						]}
						width="md"
						name="name"
					/>
					<ProFormTextArea width="md" name="desc" />
				</ModalForm>
	
				<Drawer
					width={600}
					visible={showDetail}
					onClose={() => {
						setCurrentRow(undefined);
						setShowDetail(false);
					}}
					closable={false}
				>
					{currentRow?.name && (
						<ProDescriptions<API.RuleListItem>
							column={2}
							title={currentRow?.name}
							request={async () => ({
								data: currentRow || {},
							})}
							params={{
								id: currentRow?.name,
							}}
							columns={columns as ProDescriptionsItemProps<API.RuleListItem>[]}
						/>
					)}
				</Drawer>
			</PageContainer>
		);
	};
	
	export default ${capitalHump};	
`;
	return tel;
};


module.exports = (obj) => {
	const { objName, saga, reducer, template } = obj;
	if (saga && reducer) {
		return getDefaultTel(objName);
	} else if (saga && !reducer) {
		return getSadsel(objName);
	} else if (!saga && reducer) {
		return getReducerTel(objName);
	} else {
		switch(template) {
			case "simple":
				return getBaseTel(objName);
			case "table":
				return getTablePageTel(objName);
			default:
				return getBaseTel(objName);
		}
		
	}
	// return getBaseTel(name);
};