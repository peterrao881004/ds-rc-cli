const fs = require("fs");
const { promisify } = require("util");
const writeFile = promisify(fs.writeFile);
const path = require("path");
const { changeFile } = require("../utils/tools");
const createMockData = require("./mock_data");
const createApi = require("./api");

const getTel = (obj, isJs) => {
	const { reducer, objName } = obj;
	const { capitalHump, typeName } = objName;
	const typeString = isJs ? "" : ": Generator<ForkEffect>";
	const reduxImp = isJs ? "" : ", ForkEffect";
	let tel = "";
	if (reducer) {
		tel = `import React from 'react'
import { takeEvery, put${reduxImp} } from "redux-saga/effects";
import * as api from "@src/api";
import { requestSaga } from "./request.saga";
import { FETCH_${typeName}_DATA, RECEIVE_${typeName}_DATA } from "@types";
import { Toast } from 'antd-mobile'
import CountDownText from '@components/CountDownText'

function* fetch${capitalHump}Data() {
	Toast.show({
		icon: 'loading',
		content: <CountDownText />,
		duration: 0,
	});
	try {
		const payload = yield requestSaga(api.fetch${capitalHump}Data, {
			apiParam: {},
		});
		Toast.clear();
		yield put({ type: RECEIVE_${typeName}_DATA, payload });
	} catch (e) {
		console.log(e);
	}
}

export function* watchFetch${capitalHump}Data()${typeString} {
	yield takeEvery(FETCH_${typeName}_DATA, fetch${capitalHump}Data);
}
`;
	} else {
		tel = `import React from 'react'
import { takeEvery${reduxImp} } from "redux-saga/effects";
import * as api from "@src/api";
import { requestSaga } from "./request.saga";
import { FETCH_${typeName}_DATA } from "@types";
import { Toast } from 'antd-mobile'
import CountDownText from '@components/CountDownText'

function* fetch${capitalHump}Data(param) {
	const { callback } = param.payload;
	Toast.show({
		icon: 'loading',
		content: <CountDownText />,
		duration: 0,
	});
	try {
		const payload = yield requestSaga(api.fetch${capitalHump}Data, {
			apiParam: {},
		});
		Toast.clear();
		callback && callback(payload);
	} catch (e) {
		console.log(e);
	}
}

export function* watchFetch${capitalHump}Data()${typeString} {
	yield takeEvery(FETCH_${typeName}_DATA, fetch${capitalHump}Data);
}
`;
	}
	return tel;
};

module.exports = async (obj) => {
	const { type, name, objName } = obj;
	const { littleHump, capitalHump } = objName;
	const isJs = type === "js";
	const file_src = path.resolve() + `/src/sagas/${name}.saga.${type}`;
	const index_src = path.resolve() + `/src/sagas/index.${type}`;

	try {
		await createMockData(name); //创建mock数据
		await createApi(type, objName);	//创建api
		const typeString = isJs ? "" : ": Generator<AllEffect<ForkEffect>>";

		//修改saga index文件
		changeFile(index_src, (data) => {
			let _data = data.toString();
			if (_data.indexOf(`"./${name}.saga"`) > -1) return null;
			const [imp, obj] = _data.toString().split("export");
			const objStr = obj.split("([")[1].split("])")[0];
			const tel = `import * as ${littleHump}Sagas from "./${name}.saga";
${imp}export default function* rootSaga()${typeString} {
	yield all([
		fork(${littleHump}Sagas.watchFetch${capitalHump}Data),${objStr}]);
}
`;
			return tel;
		});

		const tel = getTel(obj, isJs);
		//创建saga
		await writeFile(file_src, tel);
	} catch (e) {
		throw { msg: "saga 创建失败" };
	}
};