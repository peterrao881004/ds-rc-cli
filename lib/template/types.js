const fs = require("fs");
const { promisify } = require("util");
const writeFile = promisify(fs.writeFile);
const path = require("path");
const { changeFile } = require("../utils/tools");

const getTel = (obj, isJs) => {
	const { name, saga, reducer, objName } = obj;
	const { typeName, capitalHump } = objName;
	let tel = "";
	if (saga && reducer) {
		tel = `export const FETCH_${typeName}_DATA = "FETCH_${typeName}_DATA"; //请求${name}数据
export const RECEIVE_${typeName}_DATA = "RECEIVE_${typeName}_DATA"; //接收${name}数据
`;
		if (!isJs) {
			tel += `
interface fetch${capitalHump}DataType {
	type: typeof FETCH_${typeName}_DATA;
	payload: any;
}

export type ${capitalHump}DataType = any;

interface receive${capitalHump}DataType {
	type: typeof RECEIVE_${typeName}_DATA;
	payload: ${capitalHump}DataType;
}

export type ${capitalHump}ActionType = fetch${capitalHump}DataType | receive${capitalHump}DataType;
`;
		}
	} else if (saga && !reducer) {
		tel = `export const FETCH_${typeName}_DATA = "FETCH_${typeName}_DATA"; //请求${name}数据
`;

		if (!isJs) {
			tel += `
interface fetch${capitalHump}DataType {
	type: typeof FETCH_${typeName}_DATA;
	payload: any;
}

export type ${capitalHump}ActionType = fetch${capitalHump}DataType;
`;
		}
	} else if (!saga && reducer) {
		tel = `export const RECEIVE_${typeName}_DATA = "RECEIVE_${typeName}_DATA"; //接收${name}数据
`;
		if (!isJs) {
			tel += `
export type ${capitalHump}DataType = any;

interface receive${capitalHump}DataType {
	type: typeof RECEIVE_${typeName}_DATA;
	payload: ${capitalHump}DataType;
}

export type ${capitalHump}ActionType = receive${capitalHump}DataType;
`;
		}
	}

	return tel;
};

module.exports = async (obj) => {
	const { type, name, saga, reducer } = obj;
	const isJs = type === "js";
	if (!saga && !reducer) return;
	try {
		const str = getTel(obj, isJs);
		//判断是js还是ts模版
		if (isJs) {
			const file_src = path.resolve() + `/src/types.${type}`;
			changeFile(file_src, (data) => {
				const _data = data.toString();
				if (_data.indexOf(`"./${name}"`) > -1) return null;
				const tel = `${_data}
${str}`;
				return tel;
			});
		} else {
			const file_src = path.resolve() + `/src/typings/${name}.${type}`;
			const index_src = path.resolve() + `/src/typings/index.${type}`;
			await writeFile(file_src, str);
			changeFile(index_src, (data) => {
				const _data = data.toString();
				if (_data.indexOf(`"./${name}"`) > -1) return null;
				return `${_data}export * from "./${name}";
`;
			});
		}
	} catch (e) {
		throw { msg: "types 读写失败" };
	}
};