
const getDefaultTel = (res) => {
	const { typeName, littleHump, capitalHump, name } = res;
	const tel = `import React, { useEffect } from "react";
import { useAction, useStore } from "@hooks";
import { FETCH_${typeName}_DATA } from "@src/types";
import "./index.css";

function ${capitalHump}() {
	const fetchData = useAction(FETCH_${typeName}_DATA);
	const { ${littleHump}State } = useStore("${littleHump}Reducer");

	useEffect(() => {
		document.title = "${name}";
		fetchData();
	}, []);

	if (!${littleHump}State) return null;
	return <div>页面初始化完成:{${littleHump}State.result}</div>;
}

export default ${capitalHump};
`;
	return tel;
};

const getSadsel = (res) => {
	const { typeName, capitalHump, name } = res;
	const tel = `import React, { useEffect, useState } from "react";
import { useAction } from "@hooks";
import { FETCH_${typeName}_DATA } from "@src/types";
import "./index.css";

function ${capitalHump}() {
	const [data, setData] = useState(null);
	const fetchData = useAction(FETCH_${typeName}_DATA, { callback: (res) => setData(res) });

	useEffect(() => {
		document.title = "${name}";
		fetchData();
	}, []);

	if (!data) return null;
	return <div>页面初始化完成:{data.result}</div>;
}

export default ${capitalHump};
`;
	return tel;
};

const getReducerTel = (res) => {
	const { typeName, littleHump, capitalHump, name } = res;
	const tel = `import React, { useEffect } from "react";
import { useAction, useStore } from "@hooks";
import { RECEIVE_${typeName}_DATA } from "@src/types";
import "./index.css";

function ${capitalHump}() {
	const { ${littleHump}State } = useStore("${littleHump}Reducer");
	const updateData = useAction(RECEIVE_${typeName}_DATA);

	useEffect(() => {
		updateData({ data: "只是reducer" });
		document.title = "${name}";
	}, []);

	if (!${littleHump}State) return null;
	return <div>页面初始化完成:{${littleHump}State.data}</div>;
}

export default ${capitalHump};
`;
	return tel;
};

const getBaseTel = (res) => {
	const { capitalHump, name } = res;
	const tel = `import React, { useEffect } from "react";
import "./index.css";

function ${capitalHump}() {
	useEffect(() => {
		document.title = "${name}";
	}, []);

	return <div>页面初始化完成</div>;
}

export default ${capitalHump};
`;
	return tel;
};


module.exports = (obj) => {
	const { saga, reducer, objName } = obj;
	if (saga && reducer) {
		return getDefaultTel(objName);
	} else if (saga && !reducer) {
		return getSadsel(objName);
	} else if (!saga && reducer) {
		return getReducerTel(objName);
	} else {
		return getBaseTel(objName);
	}
	// return getDefaultTel(name);
};