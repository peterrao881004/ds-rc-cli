
const fs = require("fs");
const path = require("path");

module.exports = async (name) => {
	const file_src = path.resolve() + `/mock_data/${name}.json`;
	const tel = `{
	"code": 0,
	"msg": "OK",
	"data": {
		"result":"创建mock_data"
	}
}
	`;
	return new Promise((resolve, reject) => {
		fs.writeFile(file_src, tel, (err) => {
			if (err) {
				reject(err);
			} else {
				resolve();
			}
		});
	});
};