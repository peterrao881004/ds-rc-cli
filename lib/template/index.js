const fs = require("fs");
const path = require("path");
const util = require("util");
const chalk = require("chalk");
const log = content => console.log(chalk.red(content));

const createPage = require("./page");
const createRouter = require("./router");
const createTypes = require("./types");
const createSaga = require("./saga");
const createApi = require("./api");
const createMockData = require("./mock_data");
const createReducer = require("./reducer");

//检查项目ts还是js
const checkProject = async (name) => {
	const result = {
		type: "ts",
		page: false
	};
	const base_src = path.resolve();

	// const data = fs.readFileSync(base_src + "/package.json");
	// let _data = JSON.parse(data.toString());
	// if (_data.isTypeScript !== undefined) {
	// 	if (!_data.isTypeScript) {
	// 		result.type = "js";
	// 	}
	// } else {
	// 	//根据配置文件判断ts，js
	// 	const paths = fs.readdirSync(base_src);
	// 	if (paths.includes("jsconfig.json")) {
	// 		result.type = "js";
	// 	}
	// }

	const file_src = base_src + `/src/pages/${name}`;

	try {
		await util.promisify(fs.stat)(file_src);
		log("页面已经存在");
	} catch (e) {
		result.page = true;
	}
	return result;
};

module.exports = {
	checkProject,
	createPage,
	createRouter,
	createTypes,
	createSaga,
	createReducer,
	createApi,
	createMockData
};