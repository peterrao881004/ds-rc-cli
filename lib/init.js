const { promisify } = require("util");
const figlet = promisify(require("figlet"));

const clear = require("clear");
const chalk = require("chalk");
const log = content => console.log(chalk.green(content));

const actions = require("./actions");

module.exports = async (type, param) => {
	//打印欢迎页面
	clear();
	const data = await figlet("Welcome DS Cli");
	log(data);

	actions[type](param);
};