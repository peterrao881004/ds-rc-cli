# ds-rc-cli 文档

cli 工具用来快速生成前端项目

- webpack 4
- hot
- react 16.13
- react-router-dom
- postcss
- redux + saga

## 一.安装和配置

```
// 全局安装(必须)
$ npm link ds-rc-cli
// 卸载
$ npm unlink ds-cli
```

## 二.命令说明

```
目前提供 2 个命令：
ds-cli init    //初始化项目
ds-cli add    //新增页面
```

#### 1.生成新项目

```
$   ds-cli init
$   请输入项目名称：	//只能输入英文数字-
$   选择模版类型(默认ts)
    Mobile  //选择H5模版
    PC  //PC业务模版
$   是否用TS(y/n)(默认ts)
选择完后初始化项目(执行install，run start, open browser)，端口:3000
```

#### 2.新增页面

```
$   ds-cli add
$   请输入页面名称?：	//只能输入英文数字-,首字母必须英文
$   是否创建saga(y/n)(默认y)
$   是否创建reducer(y/n)(默认y)
```

##### 新增页面说明

- saga && reducer (常规开发)
- saga && !reducer (不需要存到 store，state 更新，如详情页)
- !saga && reducer (不需要请求，只是通过 reducer 更新交互，不常用)
- !saga && !reducer (纯页面)

### 3.模版注意事项
add 功能，对 index 文件格式，跟文件目录要求比较高，请不要随意更改

### 4.模拟启动本地下载
模板文件放置在自定义目录下，并运行一下脚本
```python
python -m SimpleHTTPServer :port(8888)
```