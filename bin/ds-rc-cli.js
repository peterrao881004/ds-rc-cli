#!/usr/bin/env node
const program = require("commander");
const { init, page } = require("../lib/inquirer");
// const page = require("../lib/actions/create_page");

//版本号
program.version(require("../package.json").version);

program.command("init")
	.description("初始化项目")
	.action(init);

program.command("add")
	.description("添加页面")
	.action(() => {
		// page({
		// 	name: "asd", saga: true, reducer: true
		// });
		page();
	});

//process当前进程 ，argv执行当前进程的参数
program.parse(process.argv);